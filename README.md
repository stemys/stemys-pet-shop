# Pet-shop frontend

This project is the skeleton of an Angular application for displaying a pet-shop based on https://bitbucket.org/stemys/stemys-interview/

You will need NodeJS version `14.x` to run the project.

If you want to add some features, please work on a branch prefixed with `iw-<initials>-`.
