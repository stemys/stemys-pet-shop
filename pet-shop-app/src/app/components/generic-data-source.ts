import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';

export abstract class GenericDataSource<T> implements DataSource<T> {

    private setupSubject = new BehaviorSubject<T[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    public loading$ = this.loadingSubject.asObservable();
    private totalElementsSubject = new BehaviorSubject<number>(undefined);
    public totalElements$ = this.totalElementsSubject.asObservable();

    constructor() {
    }

    abstract loadFunction(
        params?: any): Observable<any[]>;

    connect(collectionViewer: CollectionViewer): Observable<T[] | ReadonlyArray<T>> {
        return this.setupSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.setupSubject.complete();
        this.loadingSubject.complete();
    }

    // tslint:disable-next-line:typedef
    load(params?: any) {
        this.loadingSubject.next(true);
        this.loadFunction(params)
            .pipe(
                catchError(() => of(undefined)),
                finalize(() => this.loadingSubject.next(false)),
            )
            .subscribe(data => {
                this.setupSubject.next(data);
            });
    }
}
