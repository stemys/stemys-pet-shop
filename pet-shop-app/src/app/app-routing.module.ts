import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';


const routes: Routes = [
    {path: '', redirectTo: '/owners', pathMatch: 'full'},
    {path: 'owners', loadChildren: () => import('./owner/owner.module').then(m => m.OwnerModule), },
    {path: 'pets', loadChildren: () => import('./pets/pets.module').then(m => m.PetsModule), }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
