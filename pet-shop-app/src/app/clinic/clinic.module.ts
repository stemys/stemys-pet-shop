import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClinicRoutingModule } from './clinic-routing.module';
import { ClinicListComponent } from './clinic-list/clinic-list.component';
import { ClinicFormComponent } from './clinic-form/clinic-form.component';


@NgModule({
  declarations: [ClinicListComponent, ClinicFormComponent],
  imports: [
    CommonModule,
    ClinicRoutingModule
  ]
})
export class ClinicModule { }
