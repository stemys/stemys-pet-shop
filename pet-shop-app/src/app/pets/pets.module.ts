import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PetsRoutingModule } from './pets-routing.module';
import { PetsListComponent } from './pets-list/pets-list.component';
import { PetsFormComponent } from './pets-form/pets-form.component';


@NgModule({
  declarations: [PetsListComponent, PetsFormComponent],
  imports: [
    CommonModule,
    PetsRoutingModule
  ]
})
export class PetsModule { }
