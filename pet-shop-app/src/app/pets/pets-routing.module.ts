import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PetsListComponent} from './pets-list/pets-list.component';
import {PetsFormComponent} from './pets-form/pets-form.component';

const routes: Routes = [
    {path: '', component: PetsListComponent},
    {path: 'add', component: PetsFormComponent},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PetsRoutingModule {
}
