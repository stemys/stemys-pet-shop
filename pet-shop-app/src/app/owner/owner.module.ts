import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OwnerRoutingModule } from './owner-routing.module';
import { OwnerFormComponent } from './owner-form/owner-form.component';
import { OwnerListComponent } from './owner-list/owner-list.component';
import {MatTableModule} from '@angular/material/table';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';


@NgModule({
  declarations: [OwnerFormComponent, OwnerListComponent],
    imports: [
        CommonModule,
        OwnerRoutingModule,
        MatTableModule,
        MatButtonModule,
        MatIconModule
    ]
})
export class OwnerModule { }
