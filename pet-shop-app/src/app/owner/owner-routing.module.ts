import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {OwnerListComponent} from './owner-list/owner-list.component';
import {OwnerFormComponent} from './owner-form/owner-form.component';

const routes: Routes = [
    {path: '', component: OwnerListComponent},
    {path: 'add', component: OwnerFormComponent},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class OwnerRoutingModule {
}
