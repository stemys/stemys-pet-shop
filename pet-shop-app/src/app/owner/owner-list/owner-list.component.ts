import {Component, OnInit} from '@angular/core';
import {OwnerDataSource} from './owner-data-source';
import {OwnerService} from '../../services/owner.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-owner-list',
    templateUrl: './owner-list.component.html',
    styleUrls: ['./owner-list.component.scss']
})
export class OwnerListComponent implements OnInit {

    datasource: OwnerDataSource;
    displayedColumns: string[] = ['firstName', 'lastName', 'address', 'city', 'telephone'];

    constructor(private ownerService: OwnerService,
                private route: ActivatedRoute,
                private router: Router) {
        this.datasource = new OwnerDataSource(this.ownerService);
    }

    ngOnInit(): void {
        this.load();
    }

    // tslint:disable-next-line:typedef
    private load() {
        // tslint:disable-next-line:prefer-const
        let params;
        this.datasource.load(params);
    }

    goToForm(): void {
        this.router.navigate(['./owners/add']).then(r => {});
    }

}
