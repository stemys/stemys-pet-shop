import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {OwnerService} from '../../services/owner.service';
import {GenericDataSource} from '../../components/generic-data-source';

export class OwnerDataSource extends GenericDataSource<any> {
    constructor(private ownerService: OwnerService) {
        super();
    }

    loadFunction(params?: any): Observable<any[]> {
        return this.ownerService.get().pipe(map(ms => {
            return ms;
        }));
    }
}
