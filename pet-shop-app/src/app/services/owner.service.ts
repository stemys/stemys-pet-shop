import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class OwnerService {

    url = `${environment.apiUrl}/owner/`;

    const;
    httpOptions = {
        headers: new HttpHeaders({
            'Access-Control-Allow-Origin': '*'
        })
    };

    constructor(private http: HttpClient) {
    }

    get(): Observable<any[]> {
        return this.http.get<any[]>(`${this.url}`, this.httpOptions);
    }

}
